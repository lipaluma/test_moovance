package com.moovance.core.exceptions;

import com.moovance.core.exceptions.basics.NotFoundException;

public class DVDNotFoundException extends NotFoundException {
    public DVDNotFoundException(String name) {
        super("DVD with name '"+name+" not found");
    }
}
