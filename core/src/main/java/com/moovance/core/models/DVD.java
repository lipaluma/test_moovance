package com.moovance.core.models;

import com.moovance.core.models.promotion.NoPromotion;
import com.moovance.core.models.promotion.Promotion;

public record DVD(String name, String group, Promotion promotion) {

    public DVD(String name) {
        this(name, null, new NoPromotion());
    }
    public DVD(String name, String group) {
        this(name, group, new NoPromotion());
    }
}
