package com.moovance.core.models.promotion;

import java.util.Map;

public record PercentByGroupPromotion(Map<Integer, Double> percentByQuantities) implements Promotion {
}
