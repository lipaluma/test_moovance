package com.moovance.core.models;

import java.util.List;

public record Cart(List<DVD> listDVDs) {
}