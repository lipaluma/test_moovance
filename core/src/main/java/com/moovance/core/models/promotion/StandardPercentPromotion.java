package com.moovance.core.models.promotion;

public record StandardPercentPromotion(Double percent) implements Promotion {
}
