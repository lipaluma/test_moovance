package com.moovance.core.services;

import com.moovance.core.dao.DVDDao;
import com.moovance.core.models.DVD;

import java.util.List;

public class DVDService {

    private DVDDao dvdDao;

    public DVDService(){}
    public DVDService(DVDDao dvdDao){
        this.dvdDao = dvdDao;
    }

    public DVD findDVDByName(String name){
        return dvdDao.findByName(name);
    }
    public List<DVD> findDVDByNames(List<String> names){
       return dvdDao.findByNames(names);
    }

    public void setDvdDao(DVDDao dvdDao) {
        this.dvdDao = dvdDao;
    }
}
