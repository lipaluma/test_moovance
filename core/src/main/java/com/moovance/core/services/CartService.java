package com.moovance.core.services;

import com.moovance.core.dao.DVDDao;
import com.moovance.core.dao.LocationRateDao;
import com.moovance.core.models.Cart;
import com.moovance.core.models.DVD;
import com.moovance.core.models.promotion.PercentByGroupPromotion;
import com.moovance.core.models.promotion.StandardPercentPromotion;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class CartService {
    private LocationRateDao locationRateDao;
    private DVDService dvdService;

    private final int defaultDVDRate;

    public CartService(LocationRateDao locationRateDao) {
        this.locationRateDao = locationRateDao;
        this.defaultDVDRate = locationRateDao.getDVDDefaultRate();
    }
    public CartService(LocationRateDao locationRateDao, DVDService dvdService) {
        this(locationRateDao);
        this.dvdService = dvdService;
    }

    public Cart createCart(List<String> names) {
        names = names.stream().map(String::toLowerCase).map(StringUtils::stripAccents).toList();
        List<DVD> dvds = dvdService.findDVDByNames(names);
        return new Cart(dvds);
    }

    public Integer getTotalAmountOfCart(Cart cart) {
        Map<String, Integer> qteByGroups = getDVDQteByGroups(cart);

        double result = 0;
        for(DVD dvd : cart.listDVDs()) {
            result += switch(dvd.promotion()) {
                case StandardPercentPromotion promotion ->
                    calculateDVDRate(promotion.percent());
                case PercentByGroupPromotion promotion ->
                    getDVDRateForGroupPromotion(dvd, qteByGroups.get(StringUtils.lowerCase(dvd.group())), promotion.percentByQuantities());
                default -> defaultDVDRate;
            };
        }
        return Math.toIntExact(Math.round(result));
    }

    double calculateDVDRate(Double percent) {
        return defaultDVDRate * (1.0 - percent / 100.0);
    }

    Map<String, Integer> getDVDQteByGroups(Cart cart) {
        return cart.listDVDs().stream()
            .map(DVD::group)
            .filter(StringUtils::isNotBlank)
            .map(String::toLowerCase)
            .collect(toMap(identity(), g -> 1, Integer::sum));
    }

    Double getDVDRateForGroupPromotion(DVD dvd, Integer qteOfGroup, Map<Integer, Double> promotions) {
        int promoQteMax = promotions.keySet().stream()
            .filter(v-> v <= qteOfGroup)
            .mapToInt(v -> v)
            .max().orElse(0);
        Double percent = promotions.get(qteOfGroup);
        if(percent == null){
            percent = promotions.get(promoQteMax);
        }
        return calculateDVDRate(percent);
    }

    public void setDvdService(DVDService dvdService) {
        this.dvdService = dvdService;
    }
}
