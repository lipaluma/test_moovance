package com.moovance.core.dao;

import com.moovance.core.models.DVD;

import java.util.List;

public interface DVDDao {

    DVD findByName(String name);

    List<DVD> findByNames(List<String> names);
}
