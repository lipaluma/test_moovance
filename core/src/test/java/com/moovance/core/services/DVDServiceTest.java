package com.moovance.core.services;

import com.moovance.core.dao.DVDDao;
import com.moovance.core.models.DVD;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class DVDServiceTest {
    private static DVDDao dvdDao = Mockito.mock(DVDDao.class);
    private static DVDService dvdService = new DVDService(dvdDao);

    @BeforeAll
    public static void init(){
        when(dvdDao.findByName("La chèvre")).thenReturn(new DVD("La chèvre"));
        when(dvdDao.findByNames(List.of("La chèvre"))).thenReturn(List.of(new DVD("La chèvre")));
    }

    @Test
    void should_find_dvd_by_name() {
        DVD dvd = dvdService.findDVDByName("La chèvre");
        assertEquals("La chèvre", dvd.name());
    }

    @Test
    void should_find_dvd_by_names() {
        List<DVD> dvds = dvdService.findDVDByNames(List.of("La chèvre"));
        assertEquals("La chèvre", dvds.get(0).name());
    }
}