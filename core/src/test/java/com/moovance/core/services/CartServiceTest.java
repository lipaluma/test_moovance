package com.moovance.core.services;

import com.moovance.core.dao.DVDDao;
import com.moovance.core.dao.LocationRateDao;
import com.moovance.core.exceptions.DVDNotFoundException;
import com.moovance.core.models.Cart;
import com.moovance.core.models.DVD;
import com.moovance.core.models.promotion.PercentByGroupPromotion;
import com.moovance.core.models.promotion.StandardPercentPromotion;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;


class CartServiceTest {

    public static final int DEFAULT_DVD_RATE = 20;
    private final static Map<Integer, Double> BACK_TO_THE_FUTUR_PROMOTION = new HashMap<Integer, Double>();
    static {
        BACK_TO_THE_FUTUR_PROMOTION.put(1, 25.0);
        BACK_TO_THE_FUTUR_PROMOTION.put(2, 32.5);
        BACK_TO_THE_FUTUR_PROMOTION.put(3, 40.0);
    }
    private static final DVD LA_CHEVRE = new DVD("la chevre");
    private static final DVD LA_TAUPE = new DVD("la taupe");
    private static final DVD BACK_TO_THE_FUTUR_1 = new DVD("back to the futur 1", "back to the futur", new PercentByGroupPromotion(BACK_TO_THE_FUTUR_PROMOTION));
    private static final DVD BACK_TO_THE_FUTUR_2 = new DVD("back to the futur 2", "back to the futur", new PercentByGroupPromotion(BACK_TO_THE_FUTUR_PROMOTION));
    private static final DVD BACK_TO_THE_FUTUR_3 = new DVD("back to the futur 3", "back to the futur", new PercentByGroupPromotion(BACK_TO_THE_FUTUR_PROMOTION));
    private static final DVD IJ_ARCHE_PERDUE = new DVD("les aventuriers de l'arche perdue", "indiana jones", new StandardPercentPromotion(10.0));
    private static final DVD IJ_TEMPLE_MAUDIT = new DVD("indiana jones et le temple maudit", "indiana jones", new StandardPercentPromotion(15.0));
    private static final DVD IJ_DERNIERE_CROISADE = new DVD("indiana jones et la derniere croisade", "indiana jones", new StandardPercentPromotion(20.0));
    private static final DVD IJ_ROYAUME_CRANE = new DVD("indiana jones et le royaume du crane de cristal", "indiana jones", new StandardPercentPromotion(25.0));

    private static Map<String, DVD> data;

    private static LocationRateDao locationRateDao = Mockito.mock(LocationRateDao.class);
    private static DVDDao dvdDao = Mockito.mock(DVDDao.class);
    private static CartService cartService;

    @BeforeAll
    public static void init() {
        List<DVD> tmp = List.of(LA_CHEVRE, LA_TAUPE, BACK_TO_THE_FUTUR_1, BACK_TO_THE_FUTUR_2, BACK_TO_THE_FUTUR_3, IJ_ARCHE_PERDUE, IJ_TEMPLE_MAUDIT, IJ_DERNIERE_CROISADE, IJ_ROYAUME_CRANE);
        data = tmp.stream().collect(Collectors.toMap(DVD::name, Function.identity()));

        when(locationRateDao.getDVDDefaultRate()).thenReturn(DEFAULT_DVD_RATE);
        when(dvdDao.findByNames(anyList())).thenAnswer(inv -> {
            List<String> names = inv.getArgument(0, List.class);
            return names.stream().map(n-> data.get(n)).toList();
        });
        cartService = new CartService(locationRateDao, new DVDService(dvdDao));
    }

    @Test
    public void should_convert_all_names_to_cart() {
        Cart cart = cartService.createCart(List.of("La chèvre", "Back to the futur 1", "Back to the futur 2", "Indiana Jones et la dernière croisade"));
        List<DVD> dvds = cart.listDVDs();
        assertEquals(4, dvds.size());
        assertEquals(List.of(LA_CHEVRE, BACK_TO_THE_FUTUR_1, BACK_TO_THE_FUTUR_2, IJ_DERNIERE_CROISADE), dvds);
    }

    @Test
    public void should_calculate_dvd_rate(){
        assertEquals(18, Math.round(cartService.calculateDVDRate(10.0)));
        assertEquals(15, Math.round(cartService.calculateDVDRate(25.0)));
        assertEquals(10, Math.round(cartService.calculateDVDRate(50.0)));
        assertEquals(5, Math.round(cartService.calculateDVDRate(75.0)));
        assertEquals(2, Math.round(cartService.calculateDVDRate(90.0)));
    }

    @Test
    public void should_divide_cart_by_groups_with_quantities(){
        Cart cart = new Cart(List.of(LA_CHEVRE, LA_TAUPE,
            BACK_TO_THE_FUTUR_1, BACK_TO_THE_FUTUR_2, BACK_TO_THE_FUTUR_3,
            IJ_ARCHE_PERDUE, IJ_TEMPLE_MAUDIT, IJ_DERNIERE_CROISADE, IJ_ROYAUME_CRANE));
        Map<String, Integer> groups = cartService.getDVDQteByGroups(cart);
        assertEquals(2, groups.size());
        assertEquals(3, groups.get("back to the futur"));
        assertEquals(4, groups.get("indiana jones"));
    }

    @Test
    public void should_return_dvd_rate_for_promoted_group(){
        assertEquals(15, cartService.getDVDRateForGroupPromotion(BACK_TO_THE_FUTUR_1, 1, BACK_TO_THE_FUTUR_PROMOTION));
        assertEquals(13.5, cartService.getDVDRateForGroupPromotion(BACK_TO_THE_FUTUR_1, 2, BACK_TO_THE_FUTUR_PROMOTION));
        assertEquals(12, cartService.getDVDRateForGroupPromotion(BACK_TO_THE_FUTUR_1, 3, BACK_TO_THE_FUTUR_PROMOTION));
        assertEquals(12, cartService.getDVDRateForGroupPromotion(BACK_TO_THE_FUTUR_1, 4, BACK_TO_THE_FUTUR_PROMOTION));
        assertEquals(12, cartService.getDVDRateForGroupPromotion(BACK_TO_THE_FUTUR_1, 5, BACK_TO_THE_FUTUR_PROMOTION));
    }

    @Test
    public void should_calculate_total_cart_with_one_default_dvd() {
        Cart cart = new Cart(List.of(LA_CHEVRE));
        assertEquals(20, cartService.getTotalAmountOfCart(cart));
    }

    @Test
    public void should_calculate_total_cart_with_several_default_dvds() {
        Cart cart = new Cart(List.of(LA_CHEVRE, LA_TAUPE));
        assertEquals(40, cartService.getTotalAmountOfCart(cart));
    }

    @Test
    public void should_calculate_total_cart_with_one_promotional_dvd() {
        Cart cart = new Cart(List.of(BACK_TO_THE_FUTUR_1));
        assertEquals(15, cartService.getTotalAmountOfCart(cart));
    }

    @Test
    public void should_calculate_total_cart_with_two_promotional_dvds() {
        Cart cart = new Cart(List.of(BACK_TO_THE_FUTUR_1, BACK_TO_THE_FUTUR_2));
        assertEquals(27, cartService.getTotalAmountOfCart(cart));
    }

    @Test
    public void should_calculate_total_cart_with_three_promotional_dvds() {
        Cart cart = new Cart(List.of(BACK_TO_THE_FUTUR_1, BACK_TO_THE_FUTUR_2, BACK_TO_THE_FUTUR_3));
        assertEquals(36, cartService.getTotalAmountOfCart(cart));
    }

    @Test
    public void should_calculate_total_cart_with_four_promotional_dvds() {
        Cart cart = new Cart(List.of(BACK_TO_THE_FUTUR_1, BACK_TO_THE_FUTUR_2, BACK_TO_THE_FUTUR_3, BACK_TO_THE_FUTUR_2));
        assertEquals(48, cartService.getTotalAmountOfCart(cart));
    }

    @Test
    public void should_calculate_total_cart_with_mix_promotional_dvds() {
        Cart cart = new Cart(List.of(BACK_TO_THE_FUTUR_1, BACK_TO_THE_FUTUR_2, BACK_TO_THE_FUTUR_3, LA_CHEVRE));
        assertEquals(56, cartService.getTotalAmountOfCart(cart));
    }
}