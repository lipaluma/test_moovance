package com.moovance.api.shell;

import com.moovance.core.models.Cart;
import com.moovance.core.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@ShellComponent
public class DVDStoreShell {

    @Autowired
    private CartService cartService;

    @ShellMethod(key="amount", value = "Calculate cart amount")
    public void calculateCartAmount(String file) throws IOException {
        List<String> lines;
        try (Stream<String> streamLines = Files.lines(Paths.get(file))) {
            lines = streamLines.toList();
        }
        Cart cart = cartService.createCart(lines);
        System.out.println("amount : "+cartService.getTotalAmountOfCart(cart));
    }
}
