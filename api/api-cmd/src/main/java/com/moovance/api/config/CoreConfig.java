package com.moovance.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath*:spring-core.xml")
public class CoreConfig {
    public CoreConfig(){
        System.out.println("loading core config ...");
    }
}
