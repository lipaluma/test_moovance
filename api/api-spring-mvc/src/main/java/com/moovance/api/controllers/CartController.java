package com.moovance.api.controllers;

import com.moovance.core.dao.DVDDao;
import com.moovance.core.models.Cart;
import com.moovance.core.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("cart")
public class CartController {

    @Autowired
    private CartService cartService;
    @Autowired
    private DVDDao dvdDao;

    @PostMapping("/amount")
    public ResponseEntity<Integer> getAmount(@RequestBody List<String> dvdNames) {
        Cart cart = cartService.createCart(dvdNames);
        return ResponseEntity.ok(cartService.getTotalAmountOfCart(cart));
    }

}
