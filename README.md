# DVD Store

'DVD Store' is an example of a backend API calculating cart of DVD products handling promotions.
This application is written in JAVA 17 in a hexagonal architecture and designed by the concept of DDD.

## Installation
### Requirements
You must have some applications before testing the application:
 - jdk 17 minimum
 - maven 3

### Build
First of all, start by cloning the projet :

```bash
cd /path
git clone https://gitlab.com/lipaluma/test_moovance.git
cd test_moovance # root folder of the project
```

Then, you need to build the application from the root folder.
```bash
mvn clean install
```

### Launch application
Now, you can launch the project. There is 2 ways to launch it.

#### Mode Webapp api with spring-mvc
this mode launch a webapp application provided by spring-mvc

##### Launch By Java
Here is the command to launch the application:
```bash
java --enable-preview -jar main/webapp-api/target/moovance-dvd-store-api-1.0-SNAPSHOT.jar 
```

##### By Docker
Maybe it is easier to create a docker image of the application and run it.<br/> Launch these commands from root folder :  
```
docker build main/webapp-api/ -t dvd-store
docker run -p 8080:8080 dvd-store
```
##### Testing the API
We have integrated in the project a json postman to facilitate the call of API. The file is in this location :
``api/api-spring-mvc/src/test/resources/Moovance Test DVD Store.postman_collection.json``

Or, if you don't have postman, here is the unique endpoint accessible : <br>
```POST => http://localhost:8080/cart/amount``` <br>
accepting a JSON body listing all dvd names in an array. for example : <br/>
``["la taupe", "la chèvre", "BAck to the futur 2"]``


#### Mode Shell bash with spring-shell
The problem with the API is the syntax of the body doesn't respect perfectly the specifications that demands a style more like this:
```
la taupe
la chèvre
BAck to the futur 2
```

Then, i decided to create an another launcher like a shell command to try to resolve this problem.<br/>
Here the command to launch the shell in mode interactive :
```bash
java --enable-preview -jar main\cmd\target\moovance-dvd-store-cmd-1.0-SNAPSHOT.jar
shell:> # waiting to type a command
```
This mode will appear a shell interactive terminal that accepts a unique command 
```amount file.txt```

Finally, even with shell, it was not easy to pass parameter like the specifications describe it. I found a way but it was ugly for me :)
Then, i resolved to pass a file path as parameter : 
 - Create a file anywhere with dvd names separated like specifications. For example : ```/tmp/file.txt``` in linux, or ``c:\tmp\file.txt`` in windows
 - Type command ``amount /tmp/file.txt`` in linux or ``amount c:/tmp/file.txt`` in windows

As example, You should see result in this format : <br/>
``amount : 35``

It is possible to call the command amount directly by java with this command (without the interactive mode): 
```bash
java --enable-preview -jar main\cmd\target\moovance-dvd-store-cmd-1.0-SNAPSHOT.jar amount c:/tmp/file.txt
amount : 35
```
