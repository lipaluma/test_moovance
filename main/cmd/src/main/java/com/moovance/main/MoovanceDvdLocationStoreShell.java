package com.moovance.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.moovance.api")
public class MoovanceDvdLocationStoreShell {

    public static void main(String[] args) {
        SpringApplication.run(MoovanceDvdLocationStoreShell.class, args);
    }
}
