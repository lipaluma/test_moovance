package com.moovance.dao;

import com.moovance.core.dao.LocationRateDao;

public class LocationRateDaoImpl implements LocationRateDao {
    @Override
    public int getDVDDefaultRate() {
        return 20;
    }
}
