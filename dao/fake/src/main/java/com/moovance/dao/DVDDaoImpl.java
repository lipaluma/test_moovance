package com.moovance.dao;

import com.moovance.core.dao.DVDDao;
import com.moovance.core.exceptions.DVDNotFoundException;
import com.moovance.core.models.DVD;
import com.moovance.core.models.promotion.PercentByGroupPromotion;
import com.moovance.core.models.promotion.StandardPercentPromotion;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DVDDaoImpl implements DVDDao {
    public final static Map<Integer, Double> BACK_TO_THE_FUTUR_PROMOTION = new HashMap<>();
    static {
        BACK_TO_THE_FUTUR_PROMOTION.put(1, 25.0);
        BACK_TO_THE_FUTUR_PROMOTION.put(2, 32.5);
        BACK_TO_THE_FUTUR_PROMOTION.put(3, 40.0);
    }
    public static final DVD LA_CHEVRE = new DVD("la chevre");
    public static final DVD LA_TAUPE = new DVD("la taupe");
    public static final DVD BACK_TO_THE_FUTUR_1 = new DVD("back to the futur 1", "back to the futur", new PercentByGroupPromotion(BACK_TO_THE_FUTUR_PROMOTION));
    public static final DVD BACK_TO_THE_FUTUR_2 = new DVD("back to the futur 2", "back to the futur", new PercentByGroupPromotion(BACK_TO_THE_FUTUR_PROMOTION));
    public static final DVD BACK_TO_THE_FUTUR_3 = new DVD("back to the futur 3", "back to the futur", new PercentByGroupPromotion(BACK_TO_THE_FUTUR_PROMOTION));
    public static final DVD IJ_ARCHE_PERDUE = new DVD("les aventuriers de l'arche perdue", "indiana jones", new StandardPercentPromotion(10.0));
    public static final DVD IJ_TEMPLE_MAUDIT = new DVD("indiana jones et le temple maudit", "indiana jones", new StandardPercentPromotion(15.0));
    public static final DVD IJ_DERNIERE_CROISADE = new DVD("indiana jones et la derniere croisade", "indiana jones", new StandardPercentPromotion(20.0));
    public static final DVD IJ_ROYAUME_CRANE = new DVD("indiana jones et le royaume du crane de cristal", "indiana jones", new StandardPercentPromotion(25.0));

    private Map<String, DVD> data;
    public DVDDaoImpl(){
        List<DVD> tmp = List.of(LA_CHEVRE, LA_TAUPE, BACK_TO_THE_FUTUR_1, BACK_TO_THE_FUTUR_2, BACK_TO_THE_FUTUR_3, IJ_ARCHE_PERDUE, IJ_TEMPLE_MAUDIT, IJ_DERNIERE_CROISADE, IJ_ROYAUME_CRANE);
        data = tmp.stream().collect(Collectors.toMap(dvd -> dvd.name(), Function.identity()));
    }

    @Override
    public DVD findByName(String name) {
        DVD dvd = data.get(name);
        if (dvd == null)
            throw new DVDNotFoundException(name);
        return dvd;
    }

    @Override
    public List<DVD> findByNames(List<String> names) {
        return names.stream()
            .map(n -> findByName(n))
            .toList();
    }
}
